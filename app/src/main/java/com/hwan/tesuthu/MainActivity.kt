package com.hwan.tesuthu

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.naver.maps.geometry.LatLng
import com.naver.maps.map.*
import com.naver.maps.map.overlay.Marker
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), OnMapReadyCallback{

    private var currentLongitude : Double = -1.0
    private var currentLatitude : Double = -1.0
    private lateinit var curLocBtn : Button
    private lateinit var map : NaverMap
    private lateinit var currentLatLng : LatLng
    private lateinit var currentLoc : Location
    private lateinit var marker: Marker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val requestString = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
        if(Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,  requestString, 0)
        }
        else{
            Toast.makeText(this, "LocationManager is ready!", Toast.LENGTH_SHORT).show()
            locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                100,
                0.toFloat(),
                gpsLocationListener)

            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                100,
                0.toFloat(),
                networkLocationListener)
        }

        var mapView = MapView(this)
        map_view.addView(mapView)
        mapView.getMapAsync(this)


        curLocBtn = findViewById(R.id.curlocbtn) as Button
        curLocBtn.setOnClickListener(View.OnClickListener {
            setLocation()
        })

    }

    override fun onMapReady(map: NaverMap) {
        this.map = map
        setLocation()
    }

    private fun setLocation(){
        /*
        if(currentLatitude < 0 && currentLongitude < 0){
            currentLatitude = 37.56
            currentLongitude = 126.97
        }
         */
        Log.d("setLoc", "Latitude: " + currentLatitude + "\tLongitude: " + currentLongitude)
        currentLatLng = LatLng(currentLatitude, currentLongitude)
        currentLoc = Location("myCurrent")
        currentLoc.latitude = currentLatitude
        currentLoc.longitude = currentLongitude

        // If marker is initialized: delete the marker from map
        if(::marker.isInitialized){
            marker.map = null
        }
        marker = Marker(currentLatLng)
        marker.captionText = "현위치"
        marker.map = this.map

        var cameraUpdate:CameraUpdate = CameraUpdate.scrollAndZoomTo(currentLatLng, 15.0)
        cameraUpdate.animate(CameraAnimation.Fly, 2000)
        this.map.moveCamera(cameraUpdate)
    }

    private var gpsLocationListener = object: LocationListener{
        override fun onLocationChanged(location: Location){
            currentLongitude = location.longitude
            currentLatitude = location.latitude

        }

        @Deprecated("This callback will never be invoked.")
        override fun onStatusChanged(
            provider: String?,
            status: Int,
            extras: Bundle?
        ){}
        override fun onProviderEnabled(provider: String?){}
        override fun onProviderDisabled(provider: String?){}
    };

    private var networkLocationListener = object : LocationListener{
        override fun onLocationChanged(location: Location){
            currentLongitude = location.longitude
            currentLatitude = location.latitude

        }

        @Deprecated("This callback will never be invoked.")
        override fun onStatusChanged(
            provider: String?,
            status: Int,
            extras: Bundle?
        ){}
        override fun onProviderEnabled(provider: String?){}
        override fun onProviderDisabled(provider: String?){}
    };
}
